﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text.RegularExpressions;
using IronPython.Compiler;
using IronPython.Hosting;
using IronPython.Runtime;
using Microsoft.Scripting.Hosting;
using P21.Extensions.BusinessRule;

namespace GenericRunPythonScript
{
    public class Python
    {
        public delegate void ConfigureScopeDelegate(ScriptScope scope, ScriptEngine engine, bool initialSetup);

        /// <summary>The engine is what actually executes Python</summary>
        private static ScriptEngine _engine = null;
        /// <summary>The scope is the environment the code runs in - it contains global variables, imported modules, etc.
        /// We share it between scripts to improve performance</summary>
        private static ScriptScope _scope = null;

        /// <summary>
        /// Sets up the ScriptScope that will be shared between scripts
        /// </summary>
        private static void InitScope()
        {
            if (_engine == null)
            {
                //Setting Debug to true lets us attach the Visual Studio debugger to scripts
                var options = new Dictionary<string, object>();
                options["Debug"] = true;
                _engine = IronPython.Hosting.Python.CreateEngine(options);
            }

            try
            {
                _scope = _engine.CreateScope();

                //Some basic setup to allow the .NET integration to work correctly
                _engine.Runtime.LoadAssembly(typeof(string).Assembly);
                _scope.ImportModule("clr");
                _engine.Execute("import clr", _scope);

                //Adds support for IronPython modules implemented in C# (as opposed to pure Python libraries below)
                _engine.Execute("clr.AddReferenceToFileAndPath('IronPython.dll')", _scope);
                _engine.Execute("clr.AddReferenceToFileAndPath('IronPython.Modules.dll')", _scope);

                //Adds support for (hopefully) all of the pure Python standard library - we created this DLL from an install of Python 2.7.9
                _engine.Execute("clr.AddReferenceToFileAndPath('IronPython.StdLib.dll')", _scope);

                _engine.Execute("import sys", _scope);
                _engine.Execute("from datetime import datetime, date, timedelta", _scope);
            }
            catch 
            {
                _scope = null;
                throw;
            }
        }

        /// <summary>
        /// Executes a Python script
        /// </summary>
        /// <param name="script">The path to the script to be executed</param>
        /// <param name="variables">A dictionary of variable names and their values, to be passed to the script</param>
        /// <param name="variablesToClear">A collection of variable names to remove from the scope before execution</param>
        /// <param name="errorMessage">The text of any exception the script encountered</param>
        /// <param name="scopeCallback">A method that will do any extra setup on the scope before execution</param>
        /// <returns>The ScriptScope used to run the script.  Useful for extracting variables.</returns>
        public static ScriptScope ExecutePythonScript(string script,
            Dictionary<string, object> variables,
            IEnumerable<string> variablesToClear,
            out string errorMessage,
            ConfigureScopeDelegate scopeCallback)
        {
            errorMessage = null;
            if (string.IsNullOrEmpty(script)) return null;

            var originalCurrentDirectory = Directory.GetCurrentDirectory();

            try
            {
                var path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);

                if (path != null)
                {
                    Directory.SetCurrentDirectory(path);
                }

                var initialSetup = _scope == null;

                if (_scope == null)
                {
                    InitScope();
                }

                if (variablesToClear != null)
                {
                    foreach (var variableName in variablesToClear)
                    {
                        _scope.RemoveVariable(variableName);
                    }
                }

                if (variables != null)
                {
                    foreach (var key in variables.Keys)
                    {
                        _scope.SetVariable(key, variables[key]);

                        if (variables[key] is DateTime)
                        {
                            _engine.Execute($"{key} = datetime({key})", _scope);
                        }
                    }
                }

                // Make sure the script's folder is in the path so that it can easily include other scripts
                _engine.Execute($"sys.path.append(r'{Path.GetDirectoryName(script)}')", _scope);

                scopeCallback(_scope, _engine, initialSetup);

                var source = _engine.CreateScriptSourceFromFile(script);
                var options = (PythonCompilerOptions)_engine.GetCompilerOptions(_scope);

                options.ModuleName = "__main__";
                options.Module |= ModuleOptions.Initialize;

                var compiledSource = source.Compile(options);
                compiledSource.Execute(_scope);
            }
            catch (Exception ex)
            {
                if (_engine == null)
                {
                    throw;
                }

                var eo = _engine.GetService<ExceptionOperations>();
                errorMessage = $"Error while executing '{script}': \n\n{eo.FormatException(ex)}";
                errorMessage = Regex.Replace(errorMessage, @"\n\s+at\s.*", "", RegexOptions.Singleline);
            }
            finally
            {
                Directory.SetCurrentDirectory(originalCurrentDirectory);
            }

            return _scope;
        }

        /// <summary>
        /// Returns a Python variable name for a DataField
        /// </summary>
        /// <param name="field">The DataField to examine</param>
        /// <returns>A valid Python variable name matching the passed DataField</returns>
        public static string GetVariableName(DataField field)
        {
            var name = string.IsNullOrWhiteSpace(field.FieldAlias) ? field.FieldName : field.FieldAlias;

            return SanitizeVariableName(name);
        }

        /// <summary>
        /// Converts the passed string to a valid Python variable name
        /// </summary>
        /// <param name="name">The string to sanitize</param>
        /// <returns>A valid Python variable name</returns>
        public static string SanitizeVariableName(string name)
        {
            name = Regex.Replace(name, @"[^a-zA-Z0-9_]", "_");

            if (Regex.Match(name, @"^\d").Success)
            {
                name = "_" + name;
            }

            return name;
        }

        public static dynamic ConvertDateTimeToPython(DateTime time)
        {
            //if (_scope == null)
            //{
            //    InitScope();
            //}

            _scope.SetVariable("__dotNetDateTime", time);

            return _engine.Execute("datetime(__dotNetDateTime)", _scope);
        }
    }
}
