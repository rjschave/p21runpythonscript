﻿using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;

namespace GenericRunPythonScript
{
    /// <summary>
    /// A single parameter for a SQL query
    /// </summary>
    [DebuggerDisplay("{ToString()}")]
    public class Parameter
    {
        /// <summary>The name of the parameter, as it will appear in the query (without the @)</summary>
        public string Name;
        /// <summary>The value to be passed into the parameter</summary>
        public object Value;
        /// <summary>The direction of the parameter - Microsoft makes a distinction between input, output, input/output and return value</summary>
        public ParameterDirection Direction;

        public Parameter(string name = null, object value = null, ParameterDirection direction = ParameterDirection.Input)
        {
            Name = name;
            Value = value;
            Direction = direction;
        }

        // All of this equality code is for validating calls made to mocked methods

        public static bool operator ==(Parameter left, Parameter right)
        {
            return left?.Name == right?.Name && (left?.Value.Equals(right?.Value) ?? false) && left?.Direction == right?.Direction;
        }

        public static bool operator !=(Parameter left, Parameter right)
        {
            return !(left == right);
        }

        protected bool Equals(Parameter other)
        {
            return this == other;
        }

        public override bool Equals(object obj)
        {
            if (obj == null) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return this == (Parameter)obj;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (Name != null ? Name.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (Value != null ? Value.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ (int)Direction;
                return hashCode;
            }
        }

        public override string ToString()
        {
            return $"@{Name}: {Value} ({Direction})";
        }
        
        public static bool ParametersAreEquivalent(IEnumerable pyParameters, List<Parameter> otherParameters)
        {
            if (ReferenceEquals(pyParameters, otherParameters))
                return true;

            if (pyParameters == null || otherParameters == null)
                return false;

            var py = pyParameters.OfType<Parameter>().ToList();

            return py.Count == otherParameters.Count
                   && !py.Except(otherParameters).Any()
                   && !otherParameters.Except(py).Any();
        }
    }
}
