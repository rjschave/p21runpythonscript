﻿using System.Windows.Forms;

namespace GenericRunPythonScript
{
    public class SimpleUiProvider : ISimpleUiProvider
    {
        public DialogResult Message(string message, string title = "", MessageBoxButtons buttons = MessageBoxButtons.OK, MessageBoxIcon icon = MessageBoxIcon.Asterisk)
        {
            return MessageBox.Show(message, title, buttons, icon);
        }
    }
}
