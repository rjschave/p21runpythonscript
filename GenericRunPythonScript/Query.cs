﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;

namespace GenericRunPythonScript
{
    /// <summary>
    /// A SQL Query, for use with SqlRunner
    /// </summary>
    public class Query
    {
        /// <summary>The SQL command to be run</summary>
        public string Text;
        /// <summary>Parameters to be passed to the SQL command</summary>
        public List<Parameter> Parameters;
        /// <summary>The results returned by the query</summary>
        public DataTable Results = new DataTable();

        public Query(string text, IEnumerable<Parameter> parameters = null)
        {
            Text = text;
            Parameters = parameters?.ToList() ?? new List<Parameter>();
        }

        /// <summary>
        /// Casts the passed object to the expected type, converting null and DBNull to the type's default value
        /// </summary>
        /// <param name="obj">The object to cast</param>
        /// <returns>The passed object, cast as the expected type</returns>
        public static T ConvertFromDbVal<T>(object obj)
        {
            return obj is DBNull || obj == null ? default(T) : (T) obj;
        }

        /// <summary>
        /// Determines whether two queries are equivalent, minus whitespace
        /// </summary>
        /// <param name="query1">The first query to compare</param>
        /// <param name="query2">The second query to compare</param>
        /// <returns>Whether the two queries are equivalent</returns>
        public static bool TextsAreEquivalent(string query1, string query2)
        {
            return Regex.Replace(query1 ?? "", @"\s", "") == Regex.Replace(query2 ?? "", @"\s", "");
        }

        /// <summary>
        /// Determines whether two transactions are equivalent
        /// </summary>
        /// <param name="pyTransaction">A collection of Query objects.  IEnumerable is used because Python lists are not strongly typed</param>
        /// <param name="otherTransaction">A collection of Query objects</param>
        /// <returns>Whether the two transactions are equivalent</returns>
        public static bool TransactionsAreEquivalent(IEnumerable pyTransaction, ICollection<Query> otherTransaction)
        {
            if (ReferenceEquals(pyTransaction, otherTransaction))
                return true;

            if (pyTransaction == null || otherTransaction == null)
                return false;

            var py = pyTransaction.OfType<Query>().ToList();

            return py.Count == otherTransaction.Count
                   && !py.Except(otherTransaction).Any()
                   && !otherTransaction.Except(py).Any();
        }

        /// <summary>
        /// Compares the queries' texts and parameters
        /// </summary>
        /// <param name="left">The left hand query</param>
        /// <param name="right">The right hand query</param>
        /// <returns>Whether the two queries are equivalent</returns>
        public static bool operator ==(Query left, Query right)
        {
            return TextsAreEquivalent(left?.Text, right?.Text) && Parameter.ParametersAreEquivalent(left?.Parameters, right?.Parameters);
        }

        public static bool operator !=(Query left, Query right)
        {
            return !(left == right);
        }

        protected bool Equals(Query other)
        {
            return this == other;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (!(obj is Query)) return false;
            return this == (Query)obj;
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = Text != null ? Regex.Replace(Text, @"\s", "").GetHashCode() : 0;

                foreach (var param in Parameters)
                {
                    hashCode = (hashCode * 397) ^ (param != null ? param.GetHashCode() : 0);
                }

                return hashCode;
            }
        }
    }
}
