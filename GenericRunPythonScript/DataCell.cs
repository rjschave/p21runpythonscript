﻿using System.Data;

namespace GenericRunPythonScript
{
    /// <summary>
    /// Represents one cell in a DataTable
    /// </summary>
    public class DataCell
    {
        /// <summary>The DataTable this cell belongs to</summary>
        public DataTable Table;
        /// <summary>The DataColumn this cell belongs to</summary>
        public DataColumn Column;
        /// <summary>The DataRow this cell belongs to</summary>
        public DataRow Row;
        /// <summary>P21's "rowID" for this cells row, not necessarily the same as the index of the row in the DataTable</summary>
        public int RowId;

        /// <summary>Used to give Python easy direct access to the values in the DataTable</summary>
        public object Value
        {
            get => Row[Column];
            set => Row[Column] = value;
        }

        public DataCell(DataTable table, DataColumn col,  DataRow row, int rowId)
        {
            Table = table;
            Column = col;
            Row = row;
            RowId = rowId;
        }
    }
}
