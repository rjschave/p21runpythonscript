﻿using log4net;

namespace GenericRunPythonScript
{
    /// <summary>
    /// Used to offload null checking from the scripts - Log is null for tests
    /// </summary>
    public class LogWrapper
    {
        public ILog Log;

        public void Debug(string message)
        {
            Log?.Debug(message);
        }

        public void Info(string message)
        {
            Log?.Info(message);
        }

        public void Warn(string message)
        {
            Log?.Warn(message);
        }

        public void Error(string message)
        {
            Log?.Error(message);
        }

        public void Fatal(string message)
        {
            Log?.Fatal(message);
        }
    }
}
