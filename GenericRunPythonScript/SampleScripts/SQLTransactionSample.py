﻿# This is a simple example of how to execute multiple SQL commands in a single transaction

# Start by inserting a row into our PythonSettings table
command1Text = """INSERT INTO PythonSettings (name, value)
                  VALUES (@name, @value)"""

parameters = [Parameter('name', 'test'),
              Parameter('value', 'Some text')]

command1 = Query(command1Text, parameters)

# Now we try to get the entire contents of the PythonSettings table, but we spelled it wrong
command2Text = """SELECT id, name, value
                  FROM PythonSetting"""

command2 = Query(command2Text)

# Now let's actually execute the queries
# This should throw an error on the second query and cause the transaction to be rolled back
# There should be no changes to our PythonSettings table
SQL.ExecuteTransaction([command1, command2])

# If the second query had succeeded, we could look at its results
for row in command2.Results.Rows:
    test = row['name']