﻿"""Required: ship_to_id, sales_loc_id"""

# Looks at the address ID of the Ship To's Primary Rep and uses it as a sales location
# Meant to be triggered from customer_id and ship_to_id

if ship_to_id != '':
    query = """SELECT contacts.address_id 
               FROM ship_to_salesrep 
               INNER JOIN contacts ON contacts.id = ship_to_salesrep.salesrep_id 
               WHERE ship_to_id = @ship_to_id AND primary_salesrep = 'Y'"""

    parameters = [Parameter("ship_to_id", ship_to_id)]
    results = SQL.ExecuteSql(query, parameters)

    for row in results.Rows:
        sales_loc_id = row['address_id']
