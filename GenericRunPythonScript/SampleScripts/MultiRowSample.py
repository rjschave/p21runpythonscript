﻿"""Required: unit_quantity, unit_price"""

# This shows how to interact with fields in multi-row rules

# Values are sent as lists of cells inside a variable named after the column its cells are in
# Let's grab the first row's unit_quantity cell
firstQtyCell = unit_quantity[0]

# From here, we can change its value
firstQtyCell.Value = 1.23

# We can also look at what row, column and table the cell belongs to
qtyRow = firstQtyCell.Row
price = qtyRow['unit_price']

qtyColumn = firstQtyCell.Column
qtyColumnName = qtyColumn.ColumnName

qtyTable = firstQtyCell.Table
for row in qtyTable.Rows:
    test = row['unit_price']

# We can also find a cell from the row that triggered the rule
triggerCell = [x for x in unit_price if x.RowId == P21.Data.TriggerRow][0]

# The original DataSet passed by P21 can also be accessed and used just like you would in .NET
set = P21.Data.Set
for table in set.Tables:
    name = table.TableName