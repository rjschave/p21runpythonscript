﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Linq;

namespace GenericRunPythonScript
{
    public class SqlRunner: ISqlRunner
    {
        /// <inheritdoc />
        public string Server { get; set; } 
        /// <inheritdoc />
        public string Database { get; set; }
        /// <inheritdoc />
        public string Username { get; set; }
        /// <inheritdoc />
        public string Password { get; set; }
        /// <inheritdoc />
        public bool UseWindowsAuthentication { get; set; }

        public SqlConnection ExistingConnection { get; set; }

        /// <inheritdoc />
        public string ConnectionString
        {
            get
            {
                if (ExistingConnection != null)
                    return ExistingConnection.ConnectionString;

                var builder = new SqlConnectionStringBuilder();
                builder.DataSource = Server;
                builder.InitialCatalog = Database;
                builder.IntegratedSecurity = UseWindowsAuthentication;

                if (!UseWindowsAuthentication)
                {
                    builder.UserID = Username;
                    builder.Password = Password;
                }

                return builder.ConnectionString;
            }
        }

        /// <summary>
        /// Runs a single SQL command
        /// </summary>
        /// <param name="text">The command text to run</param>
        /// <param name="parameters">Parameters to pass to the command</param>
        /// <param name="connection">the SQL connection to use</param>
        /// <param name="transaction">The transaction the command should be a part of, if any</param>
        /// <returns>A DataTable of the command's results</returns>
        private DataTable ExecuteCommand(string text, ICollection<Parameter> parameters, DbConnection connection, DbTransaction transaction = null)
        {
            var result = new DataTable();

            var command = connection.CreateCommand();
            command.CommandText = text;
            command.Transaction = transaction;

            if (parameters != null)
            {
                foreach (var parameter in parameters)
                {
                    var param = command.CreateParameter();
                    param.ParameterName = parameter.Name;
                    param.Value = parameter.Value;
                    param.Direction = parameter.Direction;
                    command.Parameters.Add(param);
                }
            }

            using (var reader = command.ExecuteReader())
            {
                result.Load(reader);
            }

            if (parameters != null)
            {
                foreach (DbParameter param in command.Parameters)
                {
                    var parameter = parameters.FirstOrDefault(x => x.Name == param.ParameterName);

                    if (parameter != null)
                    {
                        parameter.Value = param.Value;
                    }
                }
            }

            return result;
        }

        private DbConnection GetConnection()
        {
            DbConnection connection;

            if (ExistingConnection == null)
            {
                connection = new SqlConnection();
                connection.ConnectionString = ConnectionString;
                connection.Open();
            }
            else
            {
                connection = ExistingConnection;
            }

            return connection;
        }

        /// <inheritdoc />
        public DataTable ExecuteSql(string query, IEnumerable rawParameters = null)
        {
            DataTable table;
            var parameters = rawParameters?.OfType<Parameter>().ToList();
            DbConnection connection = null;

            try
            {
                connection = GetConnection();
                table = ExecuteCommand(query, parameters, connection);
            }
            finally
            {
                if (ExistingConnection == null)
                {
                    connection?.Dispose();
                }
            }

            return table;
        }

        /// <inheritdoc />
        public void ExecuteTransaction(IEnumerable rawQueries)
        {
            if (rawQueries == null)
                return;

            var queries = rawQueries.OfType<Query>().ToList();
            DbConnection connection = null;

            try
            {
                connection = GetConnection();

                var transaction = connection.BeginTransaction();

                try
                {
                    foreach (var query in queries)
                    {
                        query.Results = ExecuteCommand(query.Text, query.Parameters, connection, transaction);
                    }
                }
                catch
                {
                    transaction.Rollback();
                    throw;
                }

                transaction.Commit();
            }
            finally
            {
                if (ExistingConnection == null)
                {
                    connection?.Dispose();
                }
            }
        }

        /// <inheritdoc />
        public int GetTableUid(string tableName)
        {
            var command = "exec @return_value = p21_get_counter @table_name,1";
            var parameters = new[] {new Parameter("table_name", tableName),
                                    new Parameter("return_value", 5, ParameterDirection.Output),};

            ExecuteSql(command, parameters);

            return (int)parameters[1].Value;
        }
    }
}
