﻿namespace GenericRunPythonScript
{
    public static class String
    {
        /// <summary>
        /// Counts the number of times a substring appears in a string
        /// </summary>
        /// <param name="original">The string to search</param>
        /// <param name="searchValue">The substring to search for</param>
        /// <returns>The number of times the substring appears in the string</returns>
        public static int CountString(this string original, string searchValue)
        {
            return (original.Length - original.Replace(searchValue, "").Length) / searchValue.Length;
        }
    }
}
