RunPythonScript is a business rule for Epicor's Prophet 21 ERP software.  It does not include any business logic itself, it is meant to execute a Python script that does the real work.  This approach can result in much simpler code and allows changes to be made without kicking everyone off of P21.

[IronPython](http://ironpython.net/) is used to embed Python code in the C# rule.  IronPython allows two way interaction between .NET code and Python code - Python can access .NET methods and objects and .NET can access Python methods and objects.

Feel free to use the issue tracker to report bugs or make suggestions, or send pull requests with direct contributions.  You can also email questions to <kbranch@kbranch.net>.

RunPythonScript is licensed under the [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0).

Installation
------------

Each P21 database that the rule will be used with needs to contain a PythonSettings table that tells the rule where to look for scripts.  You will need to create the table and add a row with a `Name` of `path`, and a `Value` of the full path to the folder you want to store scripts in.

This SQL will create an appropriate table:

```
#!sql
CREATE TABLE [dbo].[PythonSettings](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[name] [varchar](max) NOT NULL,
	[value] [varchar](max) NULL
)

INSERT INTO PythonSettings (name, value)
VALUES ('path', 'Z:\Your\Python\Folder')
```

In addition to `GenericRunPythonScript.dll`, you will need to copy the following files into the rule folder:

* `IronPython.dll`
* `IronPython.Modules.dll`
* `IronPython.StdLib.dll`
* `log4net.config`
* `log4net.dll`
* `Microsoft.Dynamic.dll`
* `Microsoft.Scripting.dll`
* `Microsoft.Scripting.Metadata.dll`

You can either get these files from the Downloads section, or by checking out a copy of the code and compiling it yourself.

Logging is done with log4net, configured with the `log4net.config` file that lives in the same folder as the rule DLL.  You should configure this file to match your environment.  By default, one log file is used for each user/database combination, to avoid file lock issues.

Using the Source Code
---------------------

The source code is written in C# using Visual Studio.  Microsoft offers the Community version of Visual Studio for free, which is generally quite capable.  The project is known to work with Visual Stuido 2017, but should also work with older versions - please report any issues you might encounter with other versions.

Everything you need to compile the project should be included, aside from the `log4net` NuGet package.  The `log4net` package is supposed to automatically be installed if it's missing, but you may need to manually install it.

Creating a Rule
---------------

At least one field must be passed to the rule with an alias of `=ScriptName`, where ScriptName is the filename of the script that you want to run.  Note that the script name should not include the path or the file extension.  This alias is only used to determine which script should be run, it is not used or modified in any way by the script.  Multiple scripts can be run by the same rule by using the same alias syntax on multiple fields.  If more than one script is specified, the execution order can be set by prefixing the alias with a number (e.g. `1=FirstScript`, `2=SecondScript`, etc.).


A script's required fields can be specified by adding a special string to the very first line of the file.  If a rule is not configured to pass all fields in the list, it will throw an error at runtime.  Any number of fields can be specified.  The string should be in this format:

```
#!python
"""Required: field_one, field_two, field_n"""
```

Interacting with P21
--------------------

Whether the script fails or not is determined by the `result` variable.  Setting `result.Success` to false will make the rule fail.  You can add a message that gets displayed to the user when the rule fails by setting `result.Message`.

####Single Row Rules
Fields get passed to the script as regular variables that are named after the field.  The variable will be named after the field's alias, if specified, otherwise it will be named after the field's name (usually the database column).  Since Python only allows alphanumeric characters and underscores in variable names, illegal characters get changed to underscores.  An alias of `This is a test!` would be available to the script as `This_is_a_test_`.  Python variables can't start with numbers, so an alias of `10` would become `_10`.

Changes made to the field variables are reflected in the actual P21 fields.  If you pass a script a field with an alias of `custid`, then set the `custid` variable to `123456`, `123456` will appear in the field in P21 once the rule is done.

####Multi Row Rules
Similar to single row rules, each field passed to the rule is associated with a Python variable.  Instead of the value being directly contained in the variable, however, each variable contains a list of `DataCell` objects, one for each row.

Changes made to the Value property of each cell will be reflected in P21.

You can also get direct access to the `DataSet` that P21 sends via `P21.Data.Set`.

Available Variables
------------------
In addition to the P21 variables described above, scripts have access to the following variables:

* `CurrentTime` is the time the rule began executing.  This is exposed as a variable so that automated tests can specify the time the rule thinks it is.
* `Log` is an object that allows the rule to write messages to the log destination(s) defined in `log4net.config`.
* `P21` is a reference to the rule itself.  Allows for more advanced access to the information that P21 sends.
* `SQL` is an object that contains methods for interacting with a SQL server.  See the Special Functions section for more information.

Built in Classes
----------------

####DataCell
The `DataCell` class represents a single cell in a `DataRow` (part of the `DataSet` that P21 passes to multi row rules).

* `Table` is the `DataTable` the cell belongs to
* `Column` is the `DataColumn` the cell belongs to
* `Row` is the `DataRow` the cell belogns to
* `RowId` is the ID that P21 has assigned to this row - useful for matching with `P21.Data.TriggerRow`
* `Value` is the actual value stored in the cell

-----

####Parameter
The `Parameter` class represents a parameter to be passed to a SQL query.

* `Name` is the name of the parameter, as it will be used in the query
* `Value` is the value that the parameter holds
* `Direction` determines what direction data moves through the parameter
    * `ParameterDirection.Input` (the default)
    * `ParameterDirection.Output`
    * `ParameterDirection.InputOutput`
    * `ParameterDirection.ReturnValue`

-----

####Query
The `Query` class represents an entire SQL query, including parameters and results.

* `Text` is the text of the SQL command to run
* `Parameters` is a list of `Parameter` objects to be used with the query
* `Results` is a `DataTable` that stores the results of the query

Built in Methods
----------------

Some methods have been added that do specific things in a simplified way.  Arguments that have a default value are optional and can be completely omitted if you want (like `attachments = None`, below).

More methods can be added by creating a new rule that inherits from the `GenericRunPythonScript` class.

-----

####UI.Message(message, title = '', buttons = MessageBoxButtons.OK, icon = MessageBoxIcon.None)
Displays a dialog box to the user.  This is part of the .NET standard library, not the Python one.

*   `message` is the message to show in the body of the dialog
*   `title` is the text to show in the dialog’s title bar
*   `buttons` control which buttons are displayed to the user
    *   `MessageBoxButtons.AbortRetryIgnore`
    *   `MessageBoxButtons.OK` (the default)
    *   `MessageBoxButtons.OKCancel`
    *   `MessageBoxButtons.RetryCancel`
    *   `MessageBoxButtons.YesNo`
    *   `MessageBoxButtons.YesNoCancel`

*   `icon` controls the icon that gets shown to the user
    *   `MessageBoxIcon.Asterisk`
    *   `MessageBoxIcon.Error`
    *   `MessageBoxIcon.Exclamation`
    *   `MessageBoxIcon.Hand`
    *   `MessageBoxIcon.Information`
    *   `MessageBoxIcon.None`
    *   `MessageBoxIcon.Question`
    *   `MessageBoxIcon.Stop`
    *   `MessageBoxIcon.Warning`
*   **Returns** a value that tells you which button the user clicked
    *   `DialogResult.Abort`
    *   `DialogResult.Cancel`
    *   `DialogResult.Ignore`
    *   `DialogResult.No`
    *   `DialogResult.None`
    *   `DialogResult.OK`
    *   `DialogResult.Retry`
    *   `DialogResult.Yes`

#####Examples

```
#!python
UI.Message("some text")

UI.Message("some text", "with a title")

clickedButton = UI.Message("some text", "with a title", MessageBoxButtons.YesNo)

if clickedButton == DialogResult.Yes:
    UI.Message("You clicked yes")

UI.Message("some text", "with a title", MessageBoxButtons.OK, MessageBoxIcon.Error)
```

More examples, just in C# rather than Python: [http://www.dotnetperls.com/messagebox-show](http://www.dotnetperls.com/messagebox-show)

-----

####SQL.ExecuteSql(query, parameters = None)
Runs arbitrary SQL against a database.  By default, queries are executed using the connection information passed by P21.

*   `query` is the text of the query you want to run
*   `parameters` is a list of `Parameter` objects for the query to use.
*   **Returns** a .NET DataTable class containing the results of the query.  More examples can be found here: [http://www.dotnetperls.com/datatable](http://www.dotnetperls.com/datatable)

#####Example
```
#!python
#Build the query
#Note that triple quotes let you use multiple lines.  The string only ends when you add another set of triple quotes
query = """select contacts.address_id from ship_to_salesrep 
           inner join contacts on contacts.id = ship_to_salesrep.salesrep_id 
           where ship_to_id = @shipToId and primary_salesrep = 'Y'"""

#Create the parameters
#Note that ship_to_id is a field from P21
params = [Parameter('shipToId', ship_to_id)]

#Actually run the query
queryResult = SQL.ExecuteSql(query, params)

#Now parse the results
#In this case we're assuming that we get a single row back with the location ID that we're after
#P21's sales_loc_id field will contain the new location ID after the rule is done
for row in queryResult.Rows:
    sales_loc_id = row['address_id']
```

-----

####SQL.ExecuteTransaction(queries)
Runs one or more queries as part of a single transaction.  If any of the queries throws an error, the entire transaction is rolled back.  Query results are stored as DataTables in the `Query` objects' `Results` field.  By default, queries are executed using the connection information passed by P21.

* `queries` is a list of `Query` objects to be executed
* **Returns** nothing - query results are stored in each `Query` object's `Results` field

#####Example
```
#!python
itemUomQuery = """INSERT INTO item_uom 
            (unit_of_measure, 
             delete_flag, 
             date_created, 
             date_last_modified, 
             last_maintained_by, 
             unit_size, 
             inv_mast_uid, 
             item_uom_uid) 
VALUES      ( @unit, 
              'N', 
              GETDATE(), 
              GETDATE(), 
              @user, 
              @unit_size, 
              @inv_mast_uid, 
              @item_uom_uid )"""

inventorySupplierXUomQuery = """INSERT INTO inventory_supplier_x_uom 
            (supplier_unit_of_measure,
             item_uom_uid, 
             inventory_supplier_uid, 
             date_created, 
             date_last_modified, 
             last_maintained_by, 
             created_by) 
VALUES      ( @unit, 
              @item_uom_uid, 
              @inventory_supplier_uid, 
              GETDATE(), 
              GETDATE(), 
              @user, 
              @user )"""

itemUomId = SQL.GetTableUid("item_uom")

parameters = [Parameter('unit', 'EA'),
              Parameter('user', global_user_id),
              Parameter('unit_size', 1),
              Parameter('inv_mast_uid', invMastId),
              Parameter('item_uom_uid', itemUomId)]

transaction.append(Query(itemUomQuery, parameters))

parameters = [Parameter('unit', 'EA'),
              Parameter('user', global_user_id),
              Parameter('inventory_supplier_uid', inventorySupplierId),
              Parameter('item_uom_uid', itemUomId1)]

transaction.append(Query(inventorySupplierXUomQuery, parameters))

SQL.ExecuteTransaction(transaction)
```

-----

####SQL.GetTableUid(tableName)
P21 sometimes uses a stored procedure to generate new UIDs for new rows.  Before inserting rows into tables that use this, you must generate a new UID to use during the insert.  Use a SQL trace to determine whether this is necessary and what table name to use.

* `tableName` is the name of the table to generate a UID for
* **Returns** a new UID integer

#####Example
```
#!python
itemUomQuery = """INSERT INTO item_uom 
            (unit_of_measure, 
             delete_flag, 
             date_created, 
             date_last_modified, 
             last_maintained_by, 
             unit_size, 
             inv_mast_uid, 
             item_uom_uid) 
VALUES      ( @unit, 
              'N', 
              GETDATE(), 
              GETDATE(), 
              @user, 
              @unit_size, 
              @inv_mast_uid, 
              @item_uom_uid )"""

itemUomId = SQL.GetTableUid("item_uom")

parameters = [Parameter('unit', 'EA'),
              Parameter('user', global_user_id),
              Parameter('unit_size', 1),
              Parameter('inv_mast_uid', invMastId),
              Parameter('item_uom_uid', itemUomId)]

SQL.ExecuteSql(itemUomQuery, parameters)
```

-----

####Log.Error(message)
Writes the passed message to the log file defined in `log4net.config`.  You can change the type of log entry by using different method names.

#####Examples
```
#!python
Log.Debug("A 'Debug' entry")
Log.Info("An 'Info' entry")
Log.Warn("A 'Warn' entry")
Log.Error("An 'Error' entry")
Log.Fatal("A 'Fatal' entry")
```

-----

####P21.GetField(name)
Gets the P21 DataField object that matches the passed variable name

* `name` is the name of the Python variable that maps to the desired field
* **Returns** a P21 DataField object that matches the passed variable name

-----

####P21.GetTriggerField()
* **Returns** the P21 DataField object matching the field that triggered the rule

-----

####P21.SetFieldUpdateOrder(colNames)
Tells P21 to update the fields in the same order as their names are passed

* `colNames` is a list of variable names, sorted in the desired update order

#####Example
```
#!python
P21.SetFieldUpdateOrder(['item_id', 'unit_price', 'qty_ordered'])
```

Adding Functionality
--------------------

You can add your own custom functionality by inheriting from the `GenericRunPythonScript` class.  For example, you could allow scripts to use custom dialog windows from your own C# library.

It's best to avoid making changes to the main GenericRunPythonScript project unless you intend to contribute them back to the project, simply because it will make it more difficult for you to use updates.

Methods of interest:

####void AddAdditionalVariables(Dictionary<string, object> variables)
This allows you to modify the variables that the script will have access to.  It gets called after the list of variables to be passed to the script has been created, but before anything has been done with them.

* `variables` is a dictionary of variable names and their values.  It contains everything that will be passed to the script, so you can add new values or modify the stock ones.

#####Example
```
#!c#
protected override void AddAdditionalVariables(Dictionary<string, object> variables)
{
    variables["MyNewVariable"] = 18;
    variables["CurrentTime"] = new DateTime(1980, 1, 1);
}
```

####void ConfigureScope(ScriptScope scope, ScriptEngine engine, bool initialSetup)
This allows you to modify the Python environment before execution - import modules, add references, run arbitrary code, etc.  It gets called after the stock setup is done.  See `Python.cs` for the stock setup, or the [IronPython project](http://ironpython.net/) for broader information about what's possible.

* `scope` is the Python environment itself.  It contains defined functions, imported modules, global variables, etc.
* `engine` is kind of analagous to the Python interpreter itself.  See the IronPython project for more information.
* `initialSetup` is whether this is the first time the scope is being configured.  You can use this to improve performance by skipping setup that has already been done.

#####Example
```
#!c#
protected override void ConfigureScope(ScriptScope scope, ScriptEngine engine, bool initialSetup)
{
    base.ConfigureScope(scope, engine, initialSetup);

    if (initialSetup)
    {
        // Add a reference to a .NET DLL
        engine.Execute("clr.AddReferenceToFileAndPath('SomeNewDll.dll')", scope);

        // Run arbitrary Python code, in this case importing the os module
        engine.Execute("import os", scope);

		// Import part of the .NET library for use in the script
        engine.Execute("from System.Windows.Forms import MessageBox", scope);
    }
}
```